const problem5 =(arg)=>{
    if((arg!==undefined) && (arg.length!==0)){
        let carsArray = []
        for(let i = 0; i<arg.length; i++){
            if(arg[i].car_year<2000){
                carsArray.push(arg[i])
            }
        }
    
        return carsArray
    }
    else{
        return []
    }
}

module.exports = problem5;