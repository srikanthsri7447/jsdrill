const problem3 = (arg)=>{
    if((arg!==undefined) && (arg.length!==0)){
        let models = []
        for(let i=0; i<arg.length; i++){
            models.push(arg[i].car_model)
        }
        let sortedOrder = models.sort()
        return sortedOrder
    }
    else{
        return []
    }
    
}

module.exports = problem3;