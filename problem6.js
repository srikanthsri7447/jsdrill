const { json } = require("stream/consumers")

const problem6 =(arg)=>{
    if((arg!==undefined) && (arg.length!==0)){
        let filteredArray = []
        for(let i=0; i<arg.length; i++){
            if((arg[i].car_make==='BMW') || (arg[i].car_make==='Audi')){
                filteredArray.push(arg[i])
            }
        }
        return filteredArray;
    }
    else{
        return []
    }
    
}

module.exports = problem6;