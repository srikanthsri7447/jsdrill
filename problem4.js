const problem4 =(arg) =>{
    if((arg!==undefined) && (arg.length!==0)){
        let yearsArray =[]
        for (let i=0; i<arg.length; i++){
            yearsArray.push(arg[i].car_year)
        }
        return yearsArray
    }
    else{
        return []
    }
    
}

module.exports = problem4;